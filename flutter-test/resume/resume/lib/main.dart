import 'package:flutter/material.dart';

Row _bildTextTitle(String lable, String des) {
  return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.alphabetic,
      children: [
        Text(lable,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                fontFamily: 'THSarabun',
                color: Colors.lightBlue[900])),
        Text(des,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 18,
                fontFamily: 'THSarabun')),
      ]);
}

Row _bildTextFont(String lable) {
  return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.alphabetic,
      children: [
        Text(lable,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
              fontFamily: 'THSarabun',
            )),
      ]);
}

Column _bildImage(String img) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Image.asset(
        img,
        width: 50,
        height: 50,
        fit: BoxFit.contain,
      ),
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Resume',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          fontFamily: 'THSarabun')),
                ),
              ])),
        ],
      ),
    );
    Widget name = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _bildTextTitle('ชื่อ: ', 'พงศกร '),
          _bildTextTitle('นามสกุล: ', 'พงษ์บ้านไร่'),
        ],
      ),
    );
    Widget name2 = Container(
      // margin: const EdgeInsets.only(left: 150,right: 150),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _bildTextTitle('ชื่อเล่น: ', 'ภูมิ '),
              _bildTextTitle('เพศ: ', 'ชาย '),
              _bildTextTitle('อายุ: ', '21ปี'),
            ],
          )),
        ],
      ),
    );
    Widget headcompSkill = Container(
      padding: const EdgeInsets.only(top: 25, left: 25),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/computerskill.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' Computer Skill',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'THSarabun',
                      color: Colors.blueAccent[400]))
            ],
          )),
        ],
      ),
    );
    Widget compSkill = Container(
      padding: const EdgeInsets.only(top: 5),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              _bildImage('images/vue.png'),
              _bildImage('images/java.png'),
              _bildImage('images/html.png'),
            ],
          )),
        ],
      ),
    );

    Widget titlePortfolio = Container(
      padding: const EdgeInsets.only(top: 25, left: 25),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/resume.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' ประวัติส่วนตัว',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'THSarabun',
                      color: Colors.blueAccent[400]))
            ],
          )),
        ],
      ),
    );
    Widget titleEducation = Container(
      padding: const EdgeInsets.only(top: 25, left: 25, bottom: 5),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/education.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' การศึกษา',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'THSarabun',
                      color: Colors.blueAccent[400]))
            ],
          )),
        ],
      ),
    );

    return MaterialApp(
        title: 'Resume',
        theme: new ThemeData(
            scaffoldBackgroundColor: const Color.fromRGBO(192, 231, 224, 0.6)),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.blueAccent[100],
          ),
          body: ListView(
            
            children: [
              
              titleSection,
              Image.asset(
                'images/profile.jpg',
                width: 240,
                height: 272,
                fit: BoxFit.contain,
              ),
              name,
              name2,
              headcompSkill,
              compSkill,
              titlePortfolio,
              Container(
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _bildTextTitle('วันเกิด: ', '26 ตุลาคม 2542'),
                  ],
                ))
              ])),
              Container(
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _bildTextTitle('เชื้อชาติ: ', 'ไทย '),
                    _bildTextTitle('สัญชาติ: ', 'ไทย '),
                    _bildTextTitle('ศาสนา: ', 'พุทธ'),
                  ],
                ))
              ])),
              Container(
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _bildTextTitle('เบอร์ติดต่อ: ', '087-6130983 '),
                  ],
                ))
              ])),
              Container(
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _bildTextTitle('อีเมล: ', 'pongsakon_pbr@hotmail.com'),
                  ],
                ))
              ])),
              titleEducation,
              Container(
                padding: const EdgeInsets.only(left: 50, ),
                
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _bildTextTitle('ประถมศึกษา ', 'โรงเรียนปรีชานุศาสน์'),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 50, ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _bildTextTitle('มัธยมศึกษา ', 'โรงเรียนชลราษฎรอำรุง'),
                  ],
                ),
              ),
               Container(
                padding: const EdgeInsets.only(left: 50, ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _bildTextTitle('อุดมศึกษา ', 'มหาวิทยาลัยบูรพา'),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
